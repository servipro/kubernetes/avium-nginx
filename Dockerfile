FROM bitnami/node:12-prod

COPY install_app.sh /install_app

RUN /install_app avium_tournament https://gitlab.com/servipro/avium_tournament.git develop

FROM frappe/erpnext-nginx:v11

COPY --from=0 /home/frappe/frappe-bench/sites/ /var/www/html/
COPY --from=0 /rsync /rsync
RUN echo -n "\navium_tournament" >> /var/www/html/apps.txt

VOLUME [ "/assets" ]

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]